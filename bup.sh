#!/bin/bash

green='\e[32m'
yellow='\e[33m'
red='\e[31m'
nocol='\e[0m'

work_dir=$(pwd)
destination=bup
if [ -d "${destination}" ]; then
    rm -r "${destination}"
fi
mkdir "${destination}"

# save ~/.password-store from program 'pass'
echo -e "${green}Backing up password store...\n...\n${nocol}"
pw_dir=~/.password-store
if [ -d "${pw_dir}" ]; then
    cp -rv "${pw_dir}" "${destination}"
else
    echo -e "${yellow}Directory ~/.password-store does not exist! no passwords backed up.\n${nocol}"
fi

# back up firefox bookmarks
echo -e "${green}Backing up Firefox bookmarks...\n...\n${nocol}"
bookmarks=$(find ~/.mozilla/firefox/*.default/bookmarkbackups | sort | tail -n1)
if [ -f "${bookmarks}" ]; then
    cp -v "${bookmarks}" "${destination}"
else
    echo -e "${yellow}No bookmarks file found.\n${nocol}"
fi

# back up dotfiles
echo -e "${green}Backing up dotfiles...\n...\n${nocol}"
declare -a dotfiles=(~/.bash_profile ~/.bashrc ~/.gitconfig);
cp -r "${dotfiles[@]}" "${destination}"

# save everything secret related
echo -e "${green}Backing up secrets...\n...\n${nocol}"
declare -a secrets=(~/.ssh/ ~/.gnupg/ ~/.pki/ ~/.nine/ ~/.local/);
cp -r  "${secrets[@]}" "${destination}"

# make a gzipped tar archive and encrypt with gpg
echo -e "${green}Making and encrypting tar.gz...\n...\n${nocol}"
tar -czvf bup.tar.gz "${destination}"
gpg -c bup.tar.gz

# remove plain folder and unencrypted archive
echo -e "${green}Cleaning up...\n...\n${nocol}"
rm -rf "${destination}"
rm bup.tar.gz
